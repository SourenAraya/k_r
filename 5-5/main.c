#include <stdio.h>
#define MAXLINE 100

void mystrncpy(char *s, char *t, int n);
void mystrncat(char s[], char t[], int n);
int mystrncmp(char *s, char *t, int n);
int mygetline(char s[], int lim);

/* Exercise 5-5. Write versions of the library functions strncpy, strncat, and
 * strncmp, which operate on at most the first n characters of their argument
 * strings. For example, strncpy(s,t,n) copies at most n characters of t to s. */

int
main()
{
	int n = 0;
	char line1[MAXLINE*2], line2[MAXLINE];

	while (mygetline(line1, MAXLINE)) {
		mygetline(line2, MAXLINE);
		printf("N = ");
		scanf("%d", &n);
		getchar();
		mystrncpy(line1, line2, n);
		printf("strncpy:\n%s\n%s\n", line1, line2);
		mystrncat(line1, line2, n);
		printf("strncat:\n%s\n%s\n", line1, line2);
		printf("strncmp:\n%s\n%s\n%d\n", line1, line2, mystrncmp(line1, line2, n));
	}

	return 0;
}

/* mystrncpy: copy first n characters from t to s */
void
mystrncpy(char *s, char *t, int n)
{
	for ( ; n > 0 && *t != '\0'; s++, t++, n--)
		*s = *t;

	*s = '\0';
}

/* mystrncat: concatenate first n characters from t to end of s
 * s must be big enough */
void
mystrncat(char s[], char t[], int n)
{
	for ( ; *s != '\0'; s++) /* find end of s */
		;

	for ( ; n > 0 && *t != '\0'; s++, t++, n--) /* copy t */
		*s = *t;

	*s = '\0';
}

/* mystrncmp: return <0 if s<t, 0 if s==t, >0 if s>t */
int
mystrncmp(char *s, char *t, int n)
{
	for ( ; n > 0 && *s == *t; s++, t++, n--)
		if (*s == '\0')
			return 0;

	return (n == 0) ? 0 : *s - *t;
}

/* mygetline: get line into s, return length */
int
mygetline(char s[], int lim)
{
	int c, i;

	i = 0;
	while (--lim > 0 && (c=getchar()) != EOF && c != '\n')
		s[i++] = c;
	if (c == '\n')
		s[i++] = c;
	s[i] = '\0';
	return i;
}
