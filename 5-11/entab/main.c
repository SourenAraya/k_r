#include <stdio.h>
#define MAXLINE 20000 /* maximum input line size */
#define TAB 8 /* default number of blanks in one tab stop */

/* states of main()'s arguments*/
#define OUT 0
#define DASH 1
#define PRE_FLAG_T 2
#define FLAG_T 3
#define ARG_T 4

int cmd_parser(const char *s);
int atoi_array(int m[], int m_size, const char s[]);
int entab(int tab, int lim);
int entab_list(const int tab_stops[], int tab_stops_size, int lim);

/* Exercise 5-11. Modify the programs entab and detab (written as exercises in
 * Chapter 1) to accept a list of tab stops as arguments. Use the default tab settings
 * if there are no arguments. */

/* entab */
/* clone of unix unexpand() utility */

int
main(int argc, char *argv[])
{
	int tabs[MAXLINE] = {TAB}, tabs_size = 1, state = OUT;

	while (*++argv) {
		switch (state = cmd_parser(*argv)) {
			case ARG_T:
				if (!(tabs_size = atoi_array(tabs, MAXLINE, *argv))) {
					printf("Main:\nError: \"%s\" wrong argument for '-t' flag\n", *argv);
					return 0;
				}
				break;
		}
	}

	if (tabs_size == 1)
		entab(tabs[0], MAXLINE);
	else
		entab_list(tabs, tabs_size, MAXLINE);

	return 0;
}

/* parse command line argument s[]
 * whether it's a flag, or argument's flag, or main argument */
int
cmd_parser(const char s[])
{
	static int state = OUT; /* default state - it's a main argument */
	int c;

	if (state != OUT)
		switch (state) {
			case FLAG_T:
				return state = ARG_T;
			default:
				state = OUT;
				break;
		}

	do {
		c = *s++;
		switch (c) {
			case '-':
				if (state == OUT)
					state = DASH;
				else
					return state = OUT;
				break;
			case 't':
				if (state == DASH)
					state = PRE_FLAG_T;
				else
					return state = OUT;
				break;
			case '\n':
			case '\0':
				if (state == PRE_FLAG_T)
					return state = FLAG_T;
				else
					return state = OUT;
				break;
			default:
				return state = OUT;
		}
	} while (c);
}

/* extract all intigers from string s[] into array m[],
 * return the size of array m[] - number of collected intigers */
#include <ctype.h>
int
atoi_array(int m[], int m_size, const char s[])
{
	int c, n, i = 0;

	while (*s && i < m_size) {
		n = 0;
		if (isdigit(*s)) {
			for (; isdigit(*s); s++)
				n = 10 * n + (*s - '0');

			m[i++] = n;
		}

		for (; (c = *s) && !isdigit(c); s++)
			;
	}

	return i;
}

/* print character ch n times */
void
print_char(char ch, int n)
{
	while (n--)
		putchar(ch);
}

/* copy not more than lim characters from input to output */
int
io_copy(int lim)
{
	int i = 0, c;

	for (i; i < lim && (c = getchar()) != EOF; i++)
		putchar(c);

	return i;
}

/* replace strings of blanks by the  minimum number of tabs
 * and blanks to achieve the same spacing.*/
/* lim - max number of characters to read from the input */
int
entab(int tab, int lim)
{
	int c;
	int i; /* number of characters that were read from the input */
	int pos; /* cursor's actual position, that counts tabs as tab_size units */
	int spaces; /* number of collected spaces */

	c = i = pos = spaces = 0;

	while (i < lim && (c = getchar()) != EOF && c != '\n') {
		if (c == ' ') {
			spaces++, pos++;
			if (pos % tab == 0) {
				putchar('\t');
				spaces = 0;
			}
		}
		else if (c == '\t') {
			putchar('\t'); 
			pos += tab - (pos % tab); /* set position to the next tab stop */
			spaces = 0;
		}
		else {
			if (spaces) {
				print_char(' ', spaces);
				spaces = 0;
			}
			putchar(c);
			pos++;
		}

		i++;
	}

	if (spaces)
		print_char(' ', spaces);
	if (c == '\n') {
		putchar('\n');
		i++;
		if (i < lim)
			i += io_copy(lim - i);
			/* print all other characters from input without modification */
	}

	return i;
}

/* replace strings of blanks by the  minimum number of tabs
 * and blanks to achieve the same spacing.*/
/* tab_stops[] - positions of columns beginnings */
/* lim - max number of characters to read from the input */
int
entab_list(const int tab_stops[], int tab_stops_size, int lim)
{
	int c;
	int i; /* number of characters that were read from the input */
	int pos; /* cursor's actual position, that counts tabs as tab's size units */
	int spaces; /* number of spaces */

	c = i = pos = spaces = 0;

	while (i < lim && tab_stops_size && (c = getchar()) != EOF && c != '\n') {
		if (c == ' ') {
			spaces++, pos++;
			if (pos == *tab_stops) {
				putchar('\t');
				spaces = 0;
			}
		}
		else if (c == '\t') {
			putchar('\t'); 
			pos = *tab_stops;
			spaces = 0;
		}
		else {
			if (spaces) {
				print_char(' ', spaces);
				spaces = 0;
			}
			putchar(c);
			pos++;
		}

		i++;
		if (pos == *tab_stops)
			tab_stops++, tab_stops_size--;
	}

	if (spaces)
		print_char(' ', spaces);

	if (c == '\n') {
		putchar('\n');
		i++;
	}

	if (!tab_stops_size && i < lim && c != EOF)
		i += io_copy(lim - i);
		/* print all other characters from input without modification */

	return i;
}
