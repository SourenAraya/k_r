#include <stdio.h>
#define MAXLINE 1000 /* maximum input line size */
#define LIM 80

int my_getline(char line[], int maxline);
int clean_line(void);

/* Exercise 1-16. Write a program to print all input lines that are longer than 80
 * characters. */

/* print longest input line */
int main()
{
	int len; /* current line length */
	char line[MAXLINE]; /* current input line */

	while ((len = my_getline(line, MAXLINE)) > 0) {
		if (len == MAXLINE - 1 && line[MAXLINE - 2] != '\n')
			len = len + clean_line();//clean line from input buffer

		if (len > LIM)
			printf("%s %d\n", line, len);
	}

	return 0;
}

/* my_getline: read a line into s, return length */
int my_getline(char s[], int lim)
{
	int c, i;

	for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c!='\n'; ++i)
		s[i] = c;

	if (c == '\n') {
		s[i] = c;
		++i;
	}
	s[i] = '\0';

	return i;
}

/* clean line from input buffer and return it's lengh */
int clean_line(void)
{
	int c, i;
	for (i = 0; (c = getchar()) != EOF && c != '\n'; ++i);
	if (c == '\n')
		++i;

	return i;
}
