#include <stdio.h>
#include <limits.h>
#define MAXLINE 100

/* Exercise 2-9. In a two’s complement number system, x &= (x-1) deletes the
 * rightmost 1-bit in x . Explain why. Use this observation to write a faster version
 * of bitcount. */

int bitcount(unsigned x);
void dec_to_bit(unsigned x, char bitwise[]);

int main()
{
	short i;
	unsigned x;
	char xbit[MAXLINE];

	printf("number of tests = ");
	scanf("%hd", &i);

	for (i; i > 0; i--) {
		printf("x = ");
		scanf("%u", &x);

		dec_to_bit(x, xbit);
		printf("x = %s\nnumber of 1 bits = %d\n", xbit, bitcount(x));

		if (i != 1) /* it is not the last test */
			putchar('\n');
	}
	return 0;
}

/* first (leftmost) and last (rightmost) bits */
const unsigned u_left = ~(~0u >> 1), u_right = ~(~0u << 1);

/* number of bits in unsigned type */
const unsigned short u_bitsize = sizeof(unsigned) * CHAR_BIT;

/* transforms decimal number x into the string which represents it's binary form */
void dec_to_bit(unsigned x, char bitwise[])
{
	int i; 
	for (i = 0; i < u_bitsize; i++, x <<= 1)
		bitwise[i] = ((x & u_left) == 0) ? '0' : '1';
	bitwise[i] = '\0';
}

/* bitcount: count 1 bits in x */
int bitcount(unsigned x)
{
	int b;
	for (b = 0; x > 0; x &= x - 1, b++);
	return b;
}
