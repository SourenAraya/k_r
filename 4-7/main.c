#include <stdio.h>
#define MAXLINE 100 /* maximum input line length */

int mygetline(char line[], int max);
int getch(void);
int ungetch(int);
int ungets(char s[]);

/* Exercise 4-7. Write a routine ungets(s) that will push back an entire string
 * onto the input. Should ungets know about buf and bufp , or should it just
 * use ungetch? */

int
main()
{
	int c;
	char s[MAXLINE];

	mygetline(s, MAXLINE);
	ungets(s);

	while ((c = getch()) != EOF)
		putchar(c);

	return 0;
}

/* mygetline: get line into s, return length */
int
mygetline(char s[], int lim)
{
	int c, i;

	i = 0;
	while (--lim > 0 && (c=getchar()) != EOF && c != '\n')
		s[i++] = c;
	if (c == '\n')
		s[i++] = c;
	s[i] = '\0';
	return i;
}


#define BUFSIZE 100

char buf[BUFSIZE]; /* buffer for ungetch */
int bufp = 0; /* next free position in buf */

int
getch(void) /* get a (possibly pushed back) character */
{
	return (bufp > 0) ? buf[--bufp] : getchar();
}

int
ungetch(int c) /* push character back on input */
{
	if (bufp >= BUFSIZE)
		return 0;
	else {
		buf[bufp++] = c;
		return 1;
	}
}

#include <string.h>

int
ungets(char s[])
{
	int i;

	for (i = strlen(s) - 1; ungetch(s[i]) && i >= 0; i--);

	return (i >= 0) ? 0 : 1; /* 0 - buffer was overflown */
}
