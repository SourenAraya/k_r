#include <stdio.h>
#define MAXLINE 100

int mygetline(char s[], int lim);
int any(char s1[], const char s2[]);

/* Exercise 2-5. Write the function any(s1,s2) , which returns the first location
 * in the string s1 where any character from the string s2 occurs, or -1 if s1
 * contains no characters from s2 . (The standard library function strpbrk does
 * the same job but returns a pointer to the location.) */

int main()
{
	char line1[MAXLINE], line2[MAXLINE];
	int len1, len2;

	/* initialize line1 */
	printf("line 1: ");
	mygetline(line1, MAXLINE);
	printf("\n"); /* in case user entered line without \n */
	
	/* initialize line2 */
	/* it may be appropriate to end entering with EOF instead of ENTER */
	printf("line 2 ('\\n' also counts): ");
	mygetline(line2, MAXLINE);
	printf("\n"); /* in case user entered line without \n */

	printf("first location of crossing s1 with any of characters from s2: %d\n", any(line1, line2));

	return 0;
}

/* getline: read a line into s, return length */
int mygetline(char s[], int lim)
{
	int c, i;

	for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c != '\n'; ++i)
		s[i] = c;
	if (c == '\n') {
		s[i] = c;
		++i;
	}
	s[i] = '\0';
	return i;
}

/* any: returns the first location in the string s1 where any character
 * from the string s2 occurs, or -1 if s1 contains no characters from s2 */
int any(char s1[], const char s2[])
{
	int i, j;

	for (i = 0; s1[i] != '\0'; i++) {
		for (j = 0; s1[i] != s2[j] && s2[j] != '\0'; j++);
		if (s2[j] != '\0')
			return i;
	}

	return -1;
}
