#include <stdio.h>
#include <stdlib.h>
#define MAXLINE 10

int mygetline(char s[], int lim);
int htoi(char s[]);

/* Exercise 2-3. Write the function htoi(s) , which converts a string of hexa-
 * decimal digits (including an optional 0x or 0X ) into its equivalent integer value.
 * The allowable digits are 0 through 9 , a through f , and A through F.*/

int main()
{
	int len;
	char line[MAXLINE];

	while ((len = mygetline(line, MAXLINE)) > 1)/* more then 1 - there was a line */
		printf("%d\n", htoi(line));

	return 0;
}

/* getline: read a line into s, return length */
int mygetline(char s[], int lim)
{
	int c, i;

	for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c != '\n'; ++i)
		s[i] = c;
	if (c == '\n') {
		s[i] = c;
		++i;
	}
	s[i] = '\0';
	return i;
}

int htoi(char s[])
{
	int i = 0, n = 0;

	if (s[i] == '0' && (s[i + 1] == 'X' || s[i + 1] == 'x'))
		i = 2;
	else {
		printf("Error 1: Wrong beginning of a Hex number - %c%c, instead of 0x or 0X\n", s[i], s[i + 1]);
		return n;
	}

	for (i; i < MAXLINE; i++)
		if ('0' <= s[i] && s[i] <= '9')
			n = n*16 + (s[i] - '0');
		else if ('A' <= s[i] && s[i] <= 'F')
			n = n*16 + (s[i] - 'A' + 10);
		else if ('a' <= s[i] && s[i] <= 'f')
			n = n*16 + (s[i] - 'a' + 10);
		else {
			if (s[i - 1] == 'x' || s[i - 1] == 'X') {
				printf("Error 2: %c - is not part of HEX number\n", s[i]);
				return n;
			}
			else
				return n; /* if number was entered correctly, the function stops when s[i] == '\n' or '\0' */
		}
}
