#include <stdio.h>
#include <string.h>

#define MAXLINE 100

/* Exercise 3-5. Write the function itob(n,s,b) that converts the integer n
 * into a base b character representation in the string s. In particular,
 * itob(n,s,16) formats n as a hexadecimal integer in s. */

void itob(int n, char s[], int b);
void reverse(char s[]);

int
main()
{
	int number = 0, base = 10;
	char line[MAXLINE];


	printf("number = ");
	scanf("%d", &number);
	printf("base (in range of [2, 36]) = ");
	scanf("%d", &base);
	if (base < 2 || base > 36) {
		printf("Error. Base out of range\n");
		return 0;
	}
	itob(number, line, base); /* convert number into string */
	printf("%s\n", line);

	return 0;
}

/* itob: convert n to characters in s in base b*/
/* b mast be in range of [2, 36] */
void
itob(int n, char s[], int b)
{
	int i, sign;
	unsigned int nn = n;

	if ((sign = n) < 0) /* record sign */
		nn = -n;         /* make nn positive */
	i = 0;
	do {       /* generate digits in reverse order */
		s[i++] = (nn % b > 9) ? nn % b - 10 + 'A' : nn % b + '0';   /* get next digit */
	} while ((nn /= b) > 0);     /* delete it */
	if (sign < 0)
		s[i++] = '-';
	s[i] = '\0';
	reverse(s);
}

/* reverse: reverse string s in place */
void 
reverse(char s[])
{
	int c, i, j;
	for (i = 0, j = strlen(s)-1; i < j; i++, j--)
		c = s[i], s[i] = s[j], s[j] = c;
}
