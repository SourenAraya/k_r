#include <stdio.h>
#define MAXLINE 120 // maximum input line size
#define TAB 8 // number of blanks in one tab stop

int my_getline(char line[], int maxline);
void copy(char to[], char from[]);
void insert_char(char line[], char ch, int n);
void p_table_header(int tab_size, int n);
void entab(char line[], short tab_size);

/* Exercise 1-21. Write a program entab that replaces strings of blanks by the
 * minimum number of tabs and blanks to achieve the same spacing. Use the
 * same tab stops as for detab. When either a tab or a single blank would suffice
 * to reach a tab stop, which should be given preference? */

int
main()
{
	int len; // current line length
	char line[MAXLINE]; // current input line

	p_table_header(TAB, 10); // print table header, colons are tab_size wide
	while ((len = my_getline(line, MAXLINE)) > 0)
		if (len > 1) { //line is not empty
			entab(line, TAB); //replace blanks with tabs
			printf("%s", line);
			putchar('\n');
		}

	return 0;
}

// my_getline: read a line into s, return length
int
my_getline(char s[], int lim)
{
	int c, i;

	for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c!='\n'; ++i)
		s[i] = c;

	if (c == '\n') {
		s[i] = c;
		++i;
	}
	s[i] = '\0';

	return i;
}

// copy: copy ’from’ into ’to’; assume to is big enough
void
copy(char to[], char from[])
{
	for (int i = 0; (to[i] = from[i]) != '\0'; ++i);
}

// insert char ch n times in the line[]
void
insert_char(char line[], char ch, int n)
{
	for (int i = 0; i < n; ++i)
		line[i] = ch;
}


// test only function
// print table header, colons are tab_size wide
void p_table_header(int tab_size, int n) {
	int i;
	for (n; n > 0; --n) {
		putchar('|');
		for (i = 0; i < tab_size - 2; ++i)
			putchar(' ');
		putchar('|');
	}
	putchar('\n');
}

/* replace strings of blanks by the  minimum number of tabs
 * and blanks to achieve the same spacing.*/
void
entab(char line[], short tab_size)
{
	int i, j;
	int jpos; //actual position, that count tabs as tab_size units
	int spaces; //number of spaces
	int next_tab; //number of symbols until next tab stop
	char orig[MAXLINE];
	copy(orig, line);

	i = j = jpos = spaces = 0;
	for (i; orig[i] != '\0' && i < MAXLINE - 1; i++) {
		next_tab = tab_size - (jpos % tab_size);
		if (orig[i] == ' ') {
			spaces++;
			if (spaces == next_tab) {
				line[j] = '\t';
				j++;
				jpos += spaces;
				spaces = 0;
			}
		}
		else if (orig[i] == '\t') {
			line[j] = '\t';
			j++;
			jpos += next_tab;
			spaces = 0;
		}
		else {
			if (spaces) {
				insert_char(line + j, '*', spaces); // '*' instead of ' ' just for the test cause
				j += spaces;
				jpos += spaces;
				spaces = 0;
			}
			line[j] = orig[i];
			j++, jpos++;
		}
	}
	line[j] = orig[i];
}
