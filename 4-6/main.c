#include <stdio.h>
#include <stdlib.h> /* for atof() */
#include <math.h> /* for fmod() */

#define MAXOP 100 /* max size of operand or operator */
#define NUMBER '0' /* signal that a number was found */
#define IDENTIFIER -5 /* signal that a name (like "cos") was found */
#define VAR_SIZE 32 /* number of variables - number of characters in alphabet */


int getop(char s[]);
void push(double);
double pop(void);
double peek(void);
void swap(void);
void clear(void);
int strindex(char s[], char t[]);

/* Exercise 4-6. Add commands for handling variables. (It’s easy to provide
 * twenty-six variables with single-letter names.) Add a variable for the most
 * recently printed value. */

/* Solution is stollen from
 * http://www.learntosolveit.com/cprogramming/Ex_4.6_calculator_variables.html
 * "5 A =" is "A = 5"*/

/* reverse Polish calculator */
int
main()
{
	int type;
	double op2, prev, var[VAR_SIZE];
	double v = 0; /* variable for last printed value */
	char s[MAXOP];


	while ((type = getop(s)) != EOF) {
		switch (type) {
			case NUMBER:
				push(atof(s));
				break;
			case IDENTIFIER:
				if (strindex(s, "cos") == 0)
					push(cos(pop()));
				else if (strindex(s, "sin") == 0)
					push(sin(pop()));
				else if (strindex(s, "pow") == 0) {
					op2 = pop();
					push(pow(pop(), op2));
				}
				else if (strindex(s, "exp") == 0)
					push(exp(pop()));
				break;
			case '+':
				push(pop() + pop());
				break;
			case '*':
				push(pop() * pop());
				break;
			case '-':
				op2 = pop();
				push(pop() - op2);
				break;
			case '/':
				op2 = pop();
				if (op2 != 0.0)
					push(pop() / op2);
				else
					printf("error: zero divisor\n");
				break;
			case '%':
				op2 = pop();
				if (op2 != 0.0)
					push(fmod(pop(), op2));
				else
					printf("error: zero divisor\n");
				break;
			case 'p':
				printf("print top element\n");
				v = peek();
				printf("\t%.8g\n", peek());
				break;
			case 'd':
				printf("duplicate two top elements\n");
				push(peek());
				break;
			case 's':
				printf("swap two top elements\n");
				swap();
				break;
			case 'c':
				printf("clear the stack\n");
				clear();
				break;
			case 'v':
				push(v); /* push last printed value */
				break;
			case '=':
				pop();
				if (prev >= 'A' && prev <= 'Z')
					var[(int)prev - 'A'] = pop();
				else
					printf("Error: there is no variable to assign a value for\n");
				break;
			case '\n':
				/* do nothing */
				break;
			default:
				if (type >= 'A' && type <= 'Z')
					push(var[type - 'A']);
				else
					printf("error: unknown command %s\n", s);
				break;
		}
		prev = type;
	}
	return 0;
}

#define MAXVAL 100 /* maximum depth of val stack */

int sp = 0; /* next free stack position */
double val[MAXVAL]; /* value stack */

/* push: push f onto value stack */
void
push(double f)
{
	if (sp < MAXVAL)
		val[sp++] = f;
	else
		printf("error: stack full, can't push %g\n", f);
}

/* pop: pop and return top value from stack */
double
pop(void)
{
	if (sp > 0)
		return val[--sp];
	else {
		printf("error: stack empty\n");
		return 0.0;
	}
}

/* peek: return top value from stack */
double
peek(void)
{
	if (sp > 0)
		return val[sp - 1];
	else {
		printf("error: stack empty\n");
		return 0.0;
	}
}

/* swap: swap top two elements from stack */
void
swap(void)
{
	if (sp < 2) {
		printf("error: number of elements in stack is less than 2\n");
		return;
	}

	double temp, temp2;
	temp = pop(), temp2 = pop();
	push(temp), push(temp2);
}

/* clear: clear the stack */
void
clear(void)
{
	sp = 0;
}

/* strindex: return index of t in s, -1 if none */
int
strindex(char s[], char t[])
{
	int i, j, k;

	for (i = 0; s[i] != '\0'; i++) {
		for (j=i, k=0; t[k]!='\0' && s[j]==t[k]; j++, k++)
			;
		if (k > 0 && t[k] == '\0')
			return i;
	}
	return -1;
}

#include <ctype.h>

int getch(void);
void ungetch(int);

/* getop: get next operator or numeric operand */
int
getop(char s[])
{
	int i, c, temp;

	while ((s[0] = c = getch()) == ' ' || c == '\t')
		;
	s[1] = '\0';
	i = 0;

	if (isalpha(c)) { /* collect characters */
		temp = c;
		while (isalpha(s[++i] = c = getch()))
			;
		s[i] = '\0';
		if (c != EOF)
			ungetch(c);

		/* return an IDENTIFIER which indicated that's a word was
		 * collected in s[] or return a single character */
		return (i > 1) ? IDENTIFIER : temp; 
	}

	if (!isdigit(c) && c != '.' && c != '-' && c != '+')
		return c; /* not a number */

	if (c == '-' || c == '+') {
		temp = getch();
		if (isdigit(temp) || temp == '.')
			s[++i] = c = temp; /* it's a signed number */
		else {
			ungetch(temp);
			return c; /* it's a sign operator */
		}
	}
	if (isdigit(c)) /* collect integer part */
		while (isdigit(s[++i] = c = getch()))
			;
	if (c == '.') /* collect fraction part */
		while (isdigit(s[++i] = c = getch()))
			;
	s[i] = '\0';
	if (c != EOF)
		ungetch(c);
	return NUMBER;
}

#define BUFSIZE 100

char buf[BUFSIZE]; /* buffer for ungetch */
int bufp = 0; /* next free position in buf */

int
getch(void) /* get a (possibly pushed back) character */
{
	return (bufp > 0) ? buf[--bufp] : getchar();
}

void
ungetch(int c) /* push character back on input */
{
	if (bufp >= BUFSIZE)
		printf("ungetch: too many characters\n");
	else
		buf[bufp++] = c;
}
