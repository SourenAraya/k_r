#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>

#define SIZE 100000

/* Exercise 3-1.Exercise 3-1. Our binary search makes two tests inside the loop, when one
 * would suffice (at the price of more tests outside). Write a version with only one
 * test inside the loop and measure the difference in run-time. */

int comp(const void *, const void *);
int binsearch(int x, int v[], int n);
int mybinsearch(int x, int v[], int n);

int main()
{
	int i, x;
	int p[SIZE];
	time_t start, end;

	printf("x = ");
	scanf("%d", &x);

	for (i = 0; i < SIZE; i++)
		p[i] = rand() % SHRT_MAX;
	qsort(p, SIZE, sizeof(int), comp);
	for (i = SIZE - 100; i < SIZE; i++)
		printf("%d\t", p[i]);
	printf("\n");

	start = time(NULL);
	i = binsearch(x, p, SIZE);
	printf("binsearch()\np[i] = %d\ni = %d\n", p[i], i);
	end = time(NULL);
	printf("Time: %f\n", difftime(end, start));

	putchar('\n');

	start = time(NULL);
	i = mybinsearch(x, p, SIZE);
	printf("mybinsearch()\np[i] = %d\ni = %d\n", p[i], i);
	end = time(NULL);
	printf("Time: %f\n", difftime(end, start));

	return 0;
}

/* binsearch: find x in v[0] <= v[1] <= ... <= v[n-1] */
int binsearch(int x, int v[], int n)
{
	int low, high, mid;
	low = 0;
	high = n - 1;
	while (low <= high) {
		for (int i = 0; i < 100; i++); /* slow down execution */
		mid = (low+high) / 2;
		if (x < v[mid])
			high = mid - 1;
		else if (x > v[mid])
			low = mid + 1;
		else
			return mid; /* found match */
	}
	for (unsigned int i = 0; i < UINT_MAX; i++); /* slow down execution */
	return -1; /* no match */
}

/* binsearch: find x in v[0] <= v[1] <= ... <= v[n-1] */
int mybinsearch(int x, int v[], int n)
{
	int low, high, mid;
	low = 0;
	high = n - 1;
	while (low < high) {
		for (int i = 0; i < 100; i++); /* slow down execution */
		mid = (low+high) / 2;
		if (x < v[mid])
			high = mid - 1;
		else
			low = mid + 1;
	}
	for (unsigned int i = 0; i < UINT_MAX; i++); /* slow down execution */
	if (x == v[mid])
		return mid;
	else if (x == v[high])
		return high;
	else
		return -1;
}

/* compare numbers */
int comp(const void *i, const void *j)
{
	  return *(int *)i - *(int *)j;
}
