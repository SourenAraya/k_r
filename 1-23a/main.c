#include <stdio.h>

/* states */
#define ESC_SINGLE_QUOTE -1
#define ESC_DOUBLE_QUOTE -2
#define OUT 0
#define SINGLE_QUOTE 1
#define DOUBLE_QUOTE 2
#define PRE_COMMENT 3
#define COMMENT 4
#define PRE_END_COMMENT 5
#define END_COMMENT 6

int c_parser(int state, char c);
void com_remove(void);

/* Exercise 1-23. Write a program to remove all comments from a C program.
 * Don’t forget to handle quoted strings and character constants properly.
 * C comments do not nest. */

int
main()
{
	/* read C-code from input and print it without C-comments */
	com_remove();

	return 0;
}

/* parse C-code - return one of the states */
int
c_parser(int state, char c)
{
	if (state == PRE_COMMENT && c != '*') /* if it is not an opening of a comment */
		state = OUT;
	else if (state == END_COMMENT)
		state = OUT;

	if (c == '\\') {
		if (state == SINGLE_QUOTE) /* if next character is a part of esc sequence like '\'' */
			state = ESC_SINGLE_QUOTE;
		else if (state == ESC_SINGLE_QUOTE) /* if curent slash is a last part of esc sequence '\\' */
			state = SINGLE_QUOTE;
		else if (state == DOUBLE_QUOTE) /* if next character is a part of esc sequence like '\'' */
			state = ESC_DOUBLE_QUOTE;
		else if (state == ESC_DOUBLE_QUOTE) /* if curent slash is a last part of esc sequence '\\' */
			state = DOUBLE_QUOTE;
	}
	else {
		if (c == '\'') {
			if (state == OUT)
				state = SINGLE_QUOTE;
			else if (state == SINGLE_QUOTE)
				state = OUT;
		}
		else if (c == '\"') {
			if (state == OUT)
				state = DOUBLE_QUOTE;
			else if (state == DOUBLE_QUOTE)
				state = OUT;
		}
		else if (c == '*') {
			if (state == PRE_COMMENT) /* if it is an opening of a comment */
				state = COMMENT; 
			else if (state == COMMENT) /* if it is a probable closing of a comment */
				state = PRE_END_COMMENT; 
		}
		else if (c == '/') {
			if (state == OUT) /* if it is a probable opening of a comment */
				state = PRE_COMMENT;
			else if (state == PRE_END_COMMENT) /* if it is a closing of a comment */
				state = END_COMMENT;
		}

		if (state == ESC_DOUBLE_QUOTE)
			state = DOUBLE_QUOTE;
		else if (state == ESC_SINGLE_QUOTE)
			state = SINGLE_QUOTE;
	}

	return state;
}

/* read C-code from input and print it without C-comments */
void
com_remove(void)
{
	int c; /* current character */
	int state = OUT; /* not in any quotes or comment */

	while ((c = getchar()) != EOF) {
		if (state == PRE_COMMENT && c != '*')
			putchar('/'); /* print delayed '/' */

		state = c_parser(state, c);

		/* print only non comment */
		/* if PRE_COMMENT - delay printing '/' until it is known whether it is comment or not */
		if (state != PRE_COMMENT && state != COMMENT && state != PRE_END_COMMENT && state != END_COMMENT)
			putchar(c);
	}
}
