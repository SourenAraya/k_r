#include <stdio.h>
#define MAXLINE 100

/*Exercise 2-6. Write a function setbits(x,p,n,y) that returns x with the n
bits that begin at position p set to the rightmost n bits of y , leaving 
the other bits unchanged.*/

unsigned setbits(unsigned x, int p, int n, unsigned y);
void dec_to_bit(unsigned x, char bitwise[]);

int main()
{
	int p, n, i;
	unsigned x, y, z;
	char zbit[MAXLINE], xbit[MAXLINE], ybit[MAXLINE];
	printf("number of tests = ");
	scanf("%d", &i);
	for (i; i > 0; i--) {
		printf("x p n y\n");
		scanf("%u %d %d %u", &x, &p, &n, &y);

		dec_to_bit(x, xbit);
		dec_to_bit(y, ybit);
		dec_to_bit(z = setbits(x, p, n, y), zbit);

		printf("x = %u = %s\n", x, xbit);
		printf("y = %u = %s\n", y, ybit);
		printf("z = %u = %s\n", z, zbit);
		printf("p = %d\n", p);
		printf("n = %d\n", n);
	}
	return 0;
}

/* returns x with the n bits that begin at position p set to the
 * rightmost n bits of y, leaving the other bits unchanged */
unsigned setbits(unsigned x, int p, int n, unsigned y)
{
	y &= ~(~0u << n); /* delete first bits, saving last n */
	y <<= (p + 1 - n); 
	x &= (~0u << (p + 1)) | ~(~0u << (p + 1 - n)); /* insert n zeros */
	return x | y; 
}

/* transform decimal number x into the string which represents it's binary form */
void dec_to_bit(unsigned x, char bitwise[])
{
	int i, size = 0;
	/* number of bits in unsigned type, which is sizeof(unsigned)*8 */
	for (unsigned temp = ~0u; temp > 0; temp <<= 1, size++); 

	for (i = 0; i < size; i++, x <<= 1)
		if ((x & ~(~0u >> 1)) == 0)
			bitwise[i] = '0';
		else
			bitwise[i] = '1';
	bitwise[i] = '\0';
}
