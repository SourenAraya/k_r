#include <stdio.h>
#define MAXLINE 300 // maximum input line size

int my_getline(char line[], int lim);
int my_strlen(char line[]);
void copy(char to[], char from[]);
void nprint(char line[], int n);
void str_fold_print(char line[], int n);

/* Exercise 1-22. Write a program to “fold” long input lines into two or more
 * shorter lines after the last non-blank character that occurs before the n-th
 * column of input. Make sure your program does something intelligent with very
 * long lines, and if there are no blanks or tabs before the specified column. */

int
main()
{
	int len; // current line length
	int n; // substring's length
	char line[MAXLINE];

	printf("N = ");
	scanf("%d", &n);
	getchar();
	while ((len = my_getline(line, MAXLINE)) > 0)
		if (len > 1) { // line is not empty
			str_fold_print(line, n); // print line as one or more lines no longer than n
			putchar('\n');
		}

	return 0;
}

/* my_getline: read a line into s, return length
   replacing EOF or '\n' with '\0' */
int
my_getline(char line[], int lim)
{
	int c, i;

	for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c != '\n'; ++i)
		line[i] = c;

	line[i] = '\0';

	return i;
}

// return length of the string line[]
int
my_strlen(char line[])
{
	int i = 0;
	for (i; line[i] != '\0'; i++);
	return i;
}

// copy: copy ’from’ into ’to’; assume to is big enough
void
copy(char to[], char from[])
{
	for (int i = 0; (to[i] = from[i]) != '\0'; ++i);
}

// print first n characters of line
void
nprint(char line[], int n)
{
	for (int i = 0; i < n && line[i] != '\0'; ++i)
		putchar(line[i]);

}

/* print folding long lines into n wide substrings
   actually n+1 wide because line[n+1] = '\n' */
void
str_fold_print(char line[], int n)
{
	int start, end; // start and end of a substring
	int temp;

	for (start = 0, end = n - 1; end + 1 < my_strlen(line); end += n) {

		temp = end;
		// get index of the last blank character in substring line[start, end]
		for (end; end > start && line[end] != ' ' && line[end] != '\t'; end--);

		if (end == start) { // if there are no blanks in substring line[start, end]
			nprint(line + start, n);
			end = temp; // return it's value
		}
		else {
			nprint(line + start, end + 1 - start);
		}
		//printf(" start = %d, end = %d", start, end);
		putchar('\n');

		start = end + 1;
	}

	// print last substring line[start, end] which length < n
	printf("%s\n", line + start);
}
