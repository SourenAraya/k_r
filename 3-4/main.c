#include <stdio.h>
#include <limits.h>
#include <string.h>

#define MAXLINE 100

/* Exercise 3-4. In a two’s complement number representation, our version of
 * itoa does not handle the largest negative number, that is, the value of n equal
 * to -(2^(wordsize-1)). Explain why not. Modify it to print that value correctly,
 * regardless of the machine on which it runs. */

/* itoa couldn't handle the largest negative number because of ranges of sign
 * types - modulo of largest_negative_number = -1*largest_positive_number + 1 */
void itoa(int n, char s[]);
void reverse(char s[]);

int
main()
{
	int number = 0;
	char line[MAXLINE];

	printf("int ranges = [%d, %d]\n", INT_MIN, INT_MAX);

	scanf("%d", &number);
	itoa(number, line); /* convert number into string */
	printf("%s\n", line);

	return 0;
}

/* itoa: convert n to characters in s */
void
itoa(int n, char s[])
{
	int i, sign;
	unsigned int nn = n;

	if ((sign = n) < 0) /* record sign */
		nn = -n;         /* make nn positive */
	i = 0;
	do {       /* generate digits in reverse order */
		s[i++] = nn % 10 + '0';   /* get next digit */
	} while ((nn /= 10) > 0);     /* delete it */
	if (sign < 0)
		s[i++] = '-';
	s[i] = '\0';
	reverse(s);
}

/* reverse: reverse string s in place */
void 
reverse(char s[])
{
	int c, i, j;
	for (i = 0, j = strlen(s)-1; i < j; i++, j--)
		c = s[i], s[i] = s[j], s[j] = c;
}
