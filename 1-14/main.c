#include <stdio.h>

#define PC_BGN 32 //ANSI printable chartacters begin
#define PC_END 126//ANSI printable chartacters end

/* Exercise 1-14. Write a program to print a histogram of the frequencies of
 * different characters in its input.*/

int main()
{
	int c;
	int p[PC_END - PC_BGN] = {0};

	while ((c = getchar()) != EOF)
		p[c - PC_BGN]++;

	for (int i = 0; i < PC_END - PC_BGN; ++i) {
		printf("%c ", i + PC_BGN);
		for (int j = 0; j < p[i]; j++)
			putchar('-');
		putchar('\n');
	}

	return 0;
}
