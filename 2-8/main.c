#include <stdio.h>
#include <limits.h>
#define MAXLINE 100

/* Exercise 2-8. Write a function rightrot(x,n) that returns the
 * value of the integer x rotated to the right by n bit positions.
 * https://stackoverflow.com/questions/25336718/kr-exercise-2-8
 * https://en.wikipedia.org/wiki/Circular_shift#Implementing_circular_shifts */

unsigned rightrot(unsigned x, int n);
void dec_to_bit(unsigned x, char bitwise[]);

int main()
{
	short i;
	unsigned n; /* number to which rotate */
	unsigned x;
	char xbit[MAXLINE];

	printf("number of tests = ");
	scanf("%u", &i);

	for (i; i > 0; i--) {
		printf("x n\n");
		scanf("%u %u", &x, &n);

		dec_to_bit(x, xbit);
		printf("x = %s = %u\n", xbit, x);

		dec_to_bit(x = rightrot(x, n), xbit);
		printf("y = %s = %u\n", xbit, x);

		printf("n = %u\n", n);

		if (i != 1) /* it is not the last test */
			putchar('\n');
	}
	return 0;
}

/* first (leftmost) and last (rightmost) bits */
const unsigned u_left = ~(~0u >> 1), u_right = ~(~0u << 1);

/* number of bits in unsigned type */
const unsigned short u_bitsize = sizeof(unsigned) * CHAR_BIT;

/* returns the value of the integer x rotated to the right by n bit positions */
/* n have to be <= u_bitsize */
unsigned rightrot(unsigned x, int n)
{
	if (n > u_bitsize) {
		printf("Error, n > %d.\n", u_bitsize);
		return 0;
	}

	return (x >> n) | (x << (u_bitsize - n));
}

/* transforms decimal number x into the string which represents it's binary form */
void dec_to_bit(unsigned x, char bitwise[])
{
	int i; 
	for (i = 0; i < u_bitsize; i++, x <<= 1)
		bitwise[i] = ((x & u_left) == 0) ? '0' : '1';
	bitwise[i] = '\0';
}
