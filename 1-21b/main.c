#include <stdio.h>
#define MAXLINE 120 // maximum input line size
#define TAB 8 // number of blanks in one tab stop

void print_only_tab(char line[], int max);
void p_table_header(int tab_size, int n);
int entab(int lim, short tab_size);

/* Exercise 1-21. Write a program entab that replaces strings of blanks by the
 * minimum number of tabs and blanks to achieve the same spacing. Use the
 * same tab stops as for detab. When either a tab or a single blank would suffice
 * to reach a tab stop, which should be given preference? */

int
main()
{
	int len; // current line length

	p_table_header(TAB, 10); // print table header, colons are tab_size wide
	while ((len = entab(MAXLINE, TAB)) > 0)
		if (len > 1) { //line is not empty
			entab(MAXLINE, TAB); //print line replacing blanks with tabs
			putchar('\n');
		}

	return 0;
}

// print char ch n times
void
print_char(char ch, int n)
{
	for (int i = 0; i < n; ++i)
		putchar(ch);
}

// test only function
// print table header, colons are tab_size wide
void p_table_header(int tab_size, int n) {
	int i;
	for (n; n > 0; --n) {
		putchar('|');
		for (i = 0; i < tab_size - 2; ++i)
			putchar(' ');
		putchar('|');
	}
	putchar('\n');
}

/* replace strings of blanks by the  minimum number of tabs
 * and blanks to achieve the same spacing.*/
int
entab(int lim, short tab_size)
{
	int c;
	int i; //number of entered characters
	int pos; //cursor's actual position, that counts tabs as tab_size units
	int spaces; //number of spaces
	int next_tab; //number of symbols until next tab stop

	i = pos = spaces = 0;
	for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c != '\n'; ++i) {
		next_tab = tab_size - (pos % tab_size);
		if (c == ' ') {
			spaces++;
			if (spaces == next_tab) {
				putchar('\t');
				pos += spaces;
				spaces = 0;
			}
		}
		else if (c == '\t') {
			putchar('\t');
			pos += next_tab;
			spaces = 0;
		}
		else {
			if (spaces) {
				print_char('*', spaces); // '*' instead of ' ' just for the test cause
				pos += spaces;
				spaces = 0;
			}
			putchar(c);
			pos++;
		}
	}

	putchar('\n');

	if (c == '\n')
		i++;

	return i;
}
