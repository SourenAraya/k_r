#include <stdio.h>

int getfloat(float *pn);

/* Exercise 5-2. Write getfloat, the floating-point analog of getint. What
 * type does getfloat return as its function value? */

int
main()
{
	float p;
	int tmp;

	while ((tmp = getfloat(&p)) != EOF && tmp != 0)
		printf("%f\n", p);

	return 0;
}

#include <ctype.h>

int getch(void);
void ungetch(int);

/* getfloat: get next float from input into *pn */
int
getfloat(float *pn)
{
	int c, sign;
	float val, power;

	while (isspace(c = getch())) /* skip white space */
		;

	if (!isdigit(c) && c != EOF && c != '+' && c != '-' && c != '.') {
		ungetch(c); /* it’s not a number */
		return 0;
	}

	sign = (c == '-') ? -1 : 1;

	if (c == '+' || c == '-')
		c = getch();

	if (!isdigit(c) && c != EOF) {
		ungetch(c); /* it’s not a number */
		return 0;
	}

	for (*pn = 0; isdigit(c); c = getch())
		*pn = 10 * *pn + (c - '0');

	if (c == '.')
		c = getch();

	for (power = 1.0; isdigit(c); c = getch()) {
		*pn = 10 * *pn + (c - '0');
		power *= 10.0;
	}

	*pn *= sign / power;

	if (c != EOF)
		ungetch(c);

	return c;
}

#define BUFSIZE 100

int buf[BUFSIZE]; /* buffer for ungetch */
int bufp = 0; /* next free position in buf */

int
getch(void) /* get a (possibly pushed back) character */
{
	return (bufp > 0) ? buf[--bufp] : getchar();
}

void
ungetch(int c) /* push character back on input */
{
	if (bufp >= BUFSIZE)
		printf("ungetch: too many characters\n");
	else
		buf[bufp++] = c;
}
