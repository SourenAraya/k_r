#include <stdio.h>

#define MAXLINE 100
#define swap(t, x, y) {t z = x; x = y, y = z;}

/* Exercise 4-14. Define a macro swap(t,x,y) that interchanges two arguments
 * of type t. (Block structure will help.) */

int
main()
{
	char c = 'v', d = 'z';
	int x = 0, y = 1;
	float f = 0.128, g = 1.064;

	printf("char c = '%c', d = '%c'\nint x = %d, y = %d\nfloat f = %f, g = %f\n", c, d, x, y, f, g);

	swap(char, c, d);
	swap(int, x, y);
	swap(float, f, g);

	putchar('\n');
	printf("char c = '%c', d = '%c'\nint x = %d, y = %d\nfloat f = %f, g = %f\n", c, d, x, y, f, g);

	return 0;
}
