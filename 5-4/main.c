#include <stdio.h>
#define MAXLINE 100

int strend(char *s, char *t);
int mystrlen(char *s);
int mygetline(char s[], int lim);

/* Exercise 5-4. Write the function strend(s,t), which returns 1 if the string
 * t occurs at the end of the string s, and zero otherwise. */

int
main()
{
	char line1[MAXLINE], line2[MAXLINE];

	while (mygetline(line1, MAXLINE)) {
		mygetline(line2, MAXLINE);
		printf("%d\n", strend(line1, line2));
	}

	return 0;
}

/* strend: return 1 if the string t occurs at the
 * end of the string s, and 0 otherwise */
int
strend(char *s, char *t)
{
	int diff = 0;

	if ((diff = mystrlen(s) - mystrlen(t)) >= 0)
		for (s += diff; *s == *t; s++, t++)
			;

	return (*s == '\0' && diff >= 0) ? 1 : 0;
}

/* mystrlen: return length of string s */
int
mystrlen(char *s)
{
	char *p = s;

	while (*p != '\0')
		p++;
	return p - s;
}

/* mygetline: get line into s, return length */
int
mygetline(char s[], int lim)
{
	int c, i;

	i = 0;
	while (--lim > 0 && (c=getchar()) != EOF && c != '\n')
		s[i++] = c;
	if (c == '\n')
		s[i++] = c;
	s[i] = '\0';
	return i;
}
