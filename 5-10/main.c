#include <stdio.h>
#include <stdlib.h> /* for atof() */

#define MAXOP 100 /* max size of operand or operator */
#define MAXLINE 100
#define NUMBER '0' /* signal that a number was found */

int getop(char s[], char line[]);
void push(double);
double pop(void);

int mygetline(char s[], int lim);
int myget_arg_line(char s[], int lim, char **argv);

/* Exercise 5-10. Write the program expr , which evaluates a reverse Polish
 * expression from the command line, where each operator or operand is a sep-
 * arate argument. For example, expr 2 3 4 + * evaluates 2 × (3+4). */

/* reverse Polish calculator */
int
main(int argc, char *argv[])
{
	int type;
	double op2;
	char line[MAXLINE], s[MAXOP];

	/* if expression wasn't entered as a command line argument */
	if (!myget_arg_line(line, MAXLINE, argv)) {
		printf("Enter expression:\n");
		mygetline(line, MAXLINE);
	}

	while ((type = getop(s, line)) != '\0') {
		switch (type) {
			case NUMBER:
				push(atof(s));
				break;
			case '+':
				push(pop() + pop());
				break;
			case '*':
				push(pop() * pop());
				break;
			case '-':
				op2 = pop();
				push(pop() - op2);
				break;
			case '/':
				op2 = pop();
				if (op2 != 0.0)
					push(pop() / op2);
				else
					printf("error: zero divisor\n");
				break;
			case '\n':
				printf("\t%.8g\n", pop());
				break;
			default:
				printf("error: unknown command \"%s\"\n", s);
				break;
		}
	}

	return 0;
}

/* mygetline: get line into s, return length */
int
mygetline(char s[], int lim)
{
	int c, i = 0;

	while (--lim > 0 && (c=getchar()) != EOF && c != '\n')
		s[i++] = c;

	if (c == '\n')
		s[i++] = c;

	s[i] = '\0';

	return i;
}

#include <string.h>
/* myget_arg_line: get all command line arguments as a
 * string into s, return length */
int
myget_arg_line(char s[], int lim, char **argv)
{
	int i = 0;

	while (*++argv != NULL && (i += 1 + strlen(*argv)) < lim) {
		strcat(s, *argv);
		strcat(s, " ");
	}
	if (i >= 1)
		s[i-1] = '\n'; /* substitute last space with newline */

	return i;
}

#define MAXVAL 100 /* maximum depth of val stack */

int sp = 0; /* next free stack position */
double val[MAXVAL]; /* value stack */

/* push: push f onto value stack */
void
push(double f)
{
	if (sp < MAXVAL)
		val[sp++] = f;
	else
		printf("error: stack full, can't push %g\n", f);
}

/* pop: pop and return top value from stack */
double
pop(void)
{
	if (sp > 0)
		return val[--sp];
	else {
		printf("error: stack empty\n");
		return 0.0;
	}
}

#include <ctype.h>

/* getop: get next operator or numeric operand */
int
getop(char s[], char line[])
{
	int i, c;
	static int line_pos = 0;

	/* skip spaces */
	while ((s[0] = c = line[line_pos++]) == ' ' || c == '\t')
		;
	s[1] = '\0';

	if (!isdigit(c) && c != '.') {
		if (c == '\0')
			line_pos = 0;
		return c; /* not a number */
	}

	i = 0;

	if (isdigit(c)) /* collect integer part */
		while (isdigit(s[++i] = c = line[line_pos++]))
			;
	if (c == '.') /* collect fraction part */
		while (isdigit(s[++i] = c = line[line_pos++]))
			;
	s[i] = '\0';

	return NUMBER;
}
