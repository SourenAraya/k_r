#include <stdio.h>
#define MAXLINE 1000 /* maximum input line size */

int my_getline(char line[], int maxline);
int removetrail(char s[], int lim);

/* Exercise 1-18. Write a program to remove trailing blanks and tabs from each
   line of input, and to delete entirely blank lines.*/

int main()
{
	int len; /* current line length */
	char line[MAXLINE]; /* current input line */

	while ((len = my_getline(line, MAXLINE)) > 0)
		if ((len = removetrail(line, MAXLINE)) > 1)
			printf("%s%d\n", line, len);

	return 0;
}

/* my_getline: read a line into s, return length */
int my_getline(char s[], int lim)
{
	int c, i;

	for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c!='\n'; ++i)
		s[i] = c;

	if (c == '\n') {
		s[i] = c;
		++i;
	}
	s[i] = '\0';

	return i;
}

/* remove trailing blanks and tabs and return lenght of a line*/
int removetrail(char s[], int lim)
{
	int i;

	for (i = 0; s[i] != '\0' && i < lim - 1; ++i);
	//get lenght, which is the index of '\0'

	for (i = i - 2; i >= 0 && (s[i] == ' ' || s[i] == '\t'); --i);
	//get index of the last non blank symbol

	s[++i] = '\n';
	s[++i] = '\0';

	return i;
}
