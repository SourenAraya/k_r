#include <stdio.h>
#define MAXLINE 120 /* maximum input line size */
#define TAB 8 /* number of blanks in one tab stop */

int my_getline(char line[], int maxline);
void copy(char to[], char from[]);
void insert_char(char line[], char ch, int n);
void detab(char line[], short tab_size);
void print_space(char line[], int maxline);

/* Exercise 1-20. Write a program detab that replaces tabs in the input with the
proper number of blanks to space to the next tab stop. Assume a fixed set of
tab stops, say every n columns. Should n be a variable or a symbolic parame-ter? */

int
main()
{
	int len; /* current line length */
	char line[MAXLINE]; /* current input line */

	while ((len = my_getline(line, MAXLINE)) > 0)
		if (len > 2) {
			detab(line, TAB);
			printf("%s", line);
			print_space(line, MAXLINE);
		}

	return 0;
}

/* my_getline: read a line into s, return length */
int
my_getline(char s[], int lim)
{
	int c, i;

	for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c!='\n'; ++i)
		s[i] = c;

	if (c == '\n') {
		s[i] = c;
		++i;
	}
	s[i] = '\0';

	return i;
}

/* copy: copy ’from’ into ’to’; assume to is big enough */
void
copy(char to[], char from[])
{
	for (int i = 0; (to[i] = from[i]) != '\0'; ++i);
}

/* insert char ch n times in the line[] */
void
insert_char(char line[], char ch, int n)
{
	for (int i = 0; i < n; ++i)
		line[i] = ch;
}

/* replace tabs in the line[] with the tab_size number of blanks */
void
detab(char line[], short tab_size)
{
	int i, j;
	int next_tab; //number of symbols until next tab stop
	char orig[MAXLINE];
	copy(orig, line);

	for (i=0,j=0; orig[i] != '\0' && i < MAXLINE - 1 && j < MAXLINE - 1; i++, j++){
		if (orig[i] == '\t'){
			next_tab = tab_size - (j % tab_size);
			insert_char(line + j, ' ', next_tab);
			j += next_tab - 1;
		}
		else
			line[j] = orig[i];
	}
	line[j] = '\0';
}


/* print ' ' as '*' */
void print_space(char line[], int maxline)
{
	int i;
	char c;
	for (i = 0; line[i] != '\0' && i < maxline - 1; ++i) {
		if (line[i] == ' ')
			putchar('*');
		else
			putchar(line[i]);
	}
}
