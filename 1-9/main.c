#include <stdio.h>

/* Exercise 1-9. Write a program to copy its input to its output, replacing each
 * string of one or more blanks by a single blank. */

int main()
{
	int c, pre_c = -2;

	while ((c = getchar()) != EOF) {
		if (!(c == ' ' && pre_c == ' '))
			putchar(c);

		pre_c = c;
	}
	
	return 0;
}
