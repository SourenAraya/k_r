#include <stdio.h>

#define MAXLINE 100

/* Exercise 4-13. Write a recursive version of the function reverse(s), which
 * reverses the string s in place. */

void reverse(char s[], int start, int end);
void swap(char v[], int i, int j);
int mygetline(char s[], int lim);

int
main()
{
	int len = 0;
	char s[MAXLINE];

	while ((len = mygetline(s, MAXLINE)) > 1) {
		reverse(s, 0, len - 1);
		printf("%s\n", s);
	}

	return 0;
}

/* reverse: reverse string s in place */
void
reverse(char s[], int start, int end)
{
	if (start >= end)
		return;

	swap(s, start, end);

	reverse(s, ++start, --end);
}

/* swap: interchange v[i] and v[j] */
void
swap(char v[], int i, int j)
{
	char temp;

	temp = v[i];
	v[i] = v[j];
	v[j] = temp;
}

/* mygetline: get line into s, return length */
int
mygetline(char s[], int lim)
{
	int c, i = 0;

	while (--lim > 0 && (c=getchar()) != EOF && c != '\n')
		s[i++] = c;

	if (c == '\n')
		s[i++] = c;

	s[i] = '\0';

	return i;
}
