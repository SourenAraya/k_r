#include <stdio.h>

#define MAXLINE 1000

int mygetline(char s[], int lim);
int mygetline2(char s[], int lim);

/* Exercise 2-2. Write a loop equivalent to the for loop above without using && or || . */

int
main()
{
	char s[MAXLINE], s2[MAXLINE];

	mygetline(s, MAXLINE - 1);
	printf("%s", s);
	mygetline2(s2, MAXLINE - 1);
	printf("%s", s2);

	return 0;
}

/* getline: read a line into s, return length */
int mygetline(char s[], int lim)
{
	int c, i;
	for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c != '\n'; ++i)
		s[i] = c;
	if (c == '\n') {
		s[i] = c;
		++i;
	}
	s[i] = '\0';
	return i;
}

/* getline: read a line into s, return length */
int mygetline2(char s[], int lim)
{
	int c, i;

	for (i = 0; i < lim - 1; ++i) {
		if ((c = getchar()) != EOF) {
			if (c != '\n')
				s[i] = c;
			else
				break;
		}
		else
			break;
	}

	if (c == '\n') {
		s[i] = c;
		++i;
	}
	s[i] = '\0';
	return i;
}
