#include <stdio.h>
#include <ctype.h>
#include <string.h>
#define MAXLINE 1000 /* maximum input line length */
#define EXP 10 /* scientific E notation */
#define EXPPP 10 /* scientific E notation */

/* Exercise 4-2. Extend atof to handle scientific notation of the form
 * 123.45e-6
 * where a floating-point number may be followed by e or E and an optionally
 * signed exponent. */

int mygetline(char line[], int max);
double atof(char s[]);
long npower(int base, int n);

/* find all lines matching pattern */
int
main()
{
	char line[MAXLINE];

	while (mygetline(line, MAXLINE) > 0)
		printf("\t%g\n", atof(line));

	return 0;
}

/* mygetline: get line into s, return length */
int
mygetline(char s[], int lim)
{
	int c, i;

	i = 0;
	while (--lim > 0 && (c=getchar()) != EOF && c != '\n')
		s[i++] = c;
	if (c == '\n')
		s[i++] = c;
	s[i] = '\0';
	return i;
}

/* atof: convert string s to double */
double atof(char s[])
{
	double val, power, exp_val;
	int i, temp, sign, exp_sign;

	for (i = 0; isspace(s[i]); i++) /* skip white space */
		;
	sign = (s[i] == '-') ? -1 : 1;
	if (s[i] == '+' || s[i] == '-')
		i++;
	for (val = 0.0; isdigit(s[i]); i++)
		val = 10.0 * val + (s[i] - '0');
	if (s[i] == '.')
		i++;
	for (power = 1.0; isdigit(s[i]); i++) {
		val = 10.0 * val + (s[i] - '0');
		power *= 10.0;
	}

	if (s[i] == 'e' || s[i] == 'E') {
		i++;

		exp_sign = (s[i] == '-') ? -1 : 1;
		if (s[i] == '+' || s[i] == '-')
			i++;

		for (exp_val = 0, temp = 0; isdigit(s[i]); i++, temp++) {
			exp_val = 10.0 * exp_val + (s[i] - '0');
		}
		if (!temp)
			exp_val = 1;

		val *= (exp_sign == 1) ? npower(EXP, exp_val) : 1.0 / npower(EXP, exp_val);
	}

	return sign * val / power;
}

/* npower: raise base to n-th power; n >= 0 */
long npower(int base, int n)
{
	long p;

	for (p = 1; n > 0; --n)
		p = p * base;
	return p;
}
