#include <stdio.h>
#define MAXLINE 100

/* Exercise 2-7. Write a function invert(x,p,n) that returns x with the n bits
 * that begin at position p inverted (i.e., 1 changed into 0 and vice versa), leaving
 * the others unchanged.*/

unsigned invert(unsigned x, int p, int n);
void dec_to_bit(unsigned x, char bitwise[]);

int main()
{
	int p, n, i;
	unsigned x, y;
	char xbit[MAXLINE], ybit[MAXLINE];
	printf("number of tests = ");
	scanf("%d", &i);
	for (i; i > 0; i--) {
		printf("x p n\n");
		scanf("%u %d %d", &x, &p, &n);

		dec_to_bit(x, xbit);
		dec_to_bit(y = invert(x, p, n), ybit);

		printf("x = %u = %s\n", x, xbit);
		printf("y = %u = %s\n", y, ybit);
		printf("p = %d\n", p);
		printf("n = %d\n", n);
	}
	return 0;
}


/* returns x with the n bits that begin at position p inverted, leaving
 * the others unchanged */
/* p = position from the right side */
unsigned invert(unsigned x, int p, int n)
{
	unsigned y = ~x;
	
	unsigned nmask = ~(~0u << (p + 1)) ^ ~(~0u << (p + 1 - n));
	/* mask n bits in position p */

	x &= ~nmask; /* delete n bits (in position p) in x */
	y &= nmask; /* delete all bits (in position p) except for n bits in y */

	return x | y;
}

/* transform decimal number x into the string which represents it's binary form */
void dec_to_bit(unsigned x, char bitwise[])
{
	int i, size = 0;
	/* number of bits in unsigned type, which is sizeof(unsigned)*8 */
	for (unsigned temp = ~0u; temp > 0; temp <<= 1, size++); 

	for (i = 0; i < size; i++, x <<= 1)
		if ((x & ~(~0u >> 1)) == 0)
			bitwise[i] = '0';
		else
			bitwise[i] = '1';
	bitwise[i] = '\0';
}
