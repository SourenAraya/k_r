#include <stdio.h>

#define MAXLINE 100
#define OUT 0
#define ESC_SEQ -1

/* Exercise 3-2. Write a function escape(s,t) that converts characters like
 * newline and tab into visible escape sequences like \n and \t as it copies the
 * string t to s . Use a switch . Write a function for the other direction as well,
 * converting escape sequences into the real characters. */

int mygetline(char s[], int lim);
void myescape(char s[], const char t[]);
void myunescape(char s[], const char t[]);

int main()
{
	char line1[MAXLINE], line2[MAXLINE];
	short i;
	printf("number of tests = ");
	scanf("%hd", &i);
	getchar(); /* ger rid of trailing '\n' */

	for (i; i > 0; i--) {
		mygetline(line1, MAXLINE);

		/* copy line1 to line2, replacing esc sequences to visual symbols */
		myescape(line2, line1);
		printf("%s\n", line2);

		/* copy line2 to line1, replacing visual symbols to esc sequences */
		myunescape(line1, line2);
		printf("%s\n", line1);
	}
	return 0;
}

/* getline: read a line into s, return length */
int mygetline(char s[], int lim)
{
	int c, i;

	for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c != '\n'; ++i)
		s[i] = c;
	if (c == '\n')
		s[i++] = c;

	s[i] = '\0';
	return i;
}

/* myescape: converts characters like newline and tab into visible
 * escape sequences like \n and \t as it copies the string t to s. */
void myescape(char s[], const char t[])
{
	int i, j;

	for (i = 0, j = 0; t[i] != '\0'; i++, j++) {
		switch (t[i]) {
			case '\n':
				s[j++] = '\\';
				s[j] = 'n';
				break;
			case '\t':
				s[j++] = '\\';
				s[j] = 't';
				break;
			default:
				s[j] = t[i];
				break;
		}
	}
	s[j] = t[i]; /* copy '\0' */
}

/* myunescape: visible escape sequences like \n and \t
 * into characters as it copies the string t to s. */
void myunescape(char s[], const char t[])
{
	int i, j, state = OUT;

	for (i = 0, j = 0; t[i] != '\0'; i++, j++) {
		if (state == OUT) {
			if (t[i] == '\\') {
				state = ESC_SEQ;
				j--;
			}
			else
				s[j] = t[i];
		}
		else if (state == ESC_SEQ) {
			if (t[i] == 'n')
				s[j] = '\n';
			else if (t[i] == 't')
				s[j] = '\t';
			else {
				s[j++] = '\\';
				s[j] = t[i];
			}
			state = OUT;
		}
	}
	s[j] = t[i]; /* copy '\0' */
}
