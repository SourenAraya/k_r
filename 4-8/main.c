#include <stdio.h>

int getch(void);
void ungetch(int);

/* Exercise 4-8. Suppose that there will never be more than one character of
 * pushback. Modify getch and ungetch accordingly. */

int
main()
{
	int c = '*';

	ungetch(c);

	while ((c = getch()) != EOF)
		putchar(c);

	return 0;
}


char buf = 0; /* buffer for ungetch */
int buf_status = 0;

int
getch(void) /* get a (possibly pushed back) character */
{
	if (buf_status == 1) {
		buf_status = 0;
		return buf;
	}
	else
		return getchar();
}

void
ungetch(int c) /* push character back on input */
{
	buf = c;
	buf_status = 1;
}
