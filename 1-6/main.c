#include <stdio.h>

/* Exercise 1-6. Verify that the expression getchar() != EOF is 0 or 1. */

/* print Fahrenheit-Celsius table */
int main()
{
	int c;

	while(c = getchar() != EOF) {
		getchar();
		printf("%d\n", c);
	}
	printf("%d\n", c);

	return 0;
}
