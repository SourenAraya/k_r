#include <stdio.h>
#define MAXLINE 1000 /* maximum input line size */

int my_getline(char line[], int maxline);
void reverse(char s[], int from, int to);

/* Exercise 1-19. Write a function reverse(s) that reverses the character
 * string s. Use it to write a program that reverses its input a line at a time. */

/* print longest input line */
int
main()
{
	int len; /* current line length */
	char line[MAXLINE]; /* current input line */

	while ((len = my_getline(line, MAXLINE)) > 0)
		if (len > 2) {
			reverse(line, 0, len - 2);//line[len-1]='\n'
			printf("%s", line);
		}

	return 0;
}

/* my_getline: read a line into s, return length */
int
my_getline(char s[], int lim)
{
	int c, i;

	for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c!='\n'; ++i)
		s[i] = c;

	if (c == '\n') {
		s[i] = c;
		++i;
	}
	s[i] = '\0';

	return i;
}

/* reverses the character string s. */
/* 'to' should be < len - 1 */
void
reverse(char s[], int from, int to)
{
	int c;
	for (from, to; from < to; ++from, --to) {
		c = s[from];
		s[from] = s[to];
		s[to] = c;
	}
}
