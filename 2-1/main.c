#include <stdio.h>
#include <limits.h>

/* Exercise 2-1. Write a program to determine the ranges of char , short , int ,
 * and long variables, both signed and unsigned , by printing appropriate
 * values from standard headers and by direct computation. Harder if you compute
 * them: determine the ranges of the various floating-point types. */

int
main()
{
	/* all bitwise expressions are valid in two's complement interpretation */

	printf("char \t\t%d, %d\n", CHAR_MIN, CHAR_MAX);
	printf("char \t\t%d, %d\n", ~((unsigned char) ~0 >> 1), (unsigned char) ~0 >> 1);
	printf("signed char \t%d, %d\n", SCHAR_MIN, SCHAR_MAX);
	printf("signed char \t%d, %d\n", ~((unsigned char) ~0 >> 1), (unsigned char) ~0 >> 1);
	printf("unsigned char \t%d, %d\n", 0, UCHAR_MAX);
	printf("unsigned char \t%d, %d\n", 0, (unsigned char) ~0);

	printf("short \t\t%hd, %hd\n", SHRT_MIN, SHRT_MAX);
	printf("short \t\t%hd, %hd\n", ~((unsigned short) ~0 >> 1), (unsigned short) ~0 >> 1);
	printf("unsigned short \t%hd, %hd\n", 0, USHRT_MAX);
	printf("unsigned short \t%hd, %hd\n", 0, (unsigned short) ~0);

	printf("int \t\t%d, %d\n", INT_MIN, INT_MAX);
	printf("int \t\t%d, %d\n", ~(~0u>>1), ~0u>>1);
	printf("unsigned int \t%u, %u\n", 0, UINT_MAX);
	printf("unsigned int \t%u, %u\n", 0, ~0u);

	printf("long \t\t%ld, %ld\n", LONG_MIN, LONG_MAX);
	printf("long \t\t%ld, %ld\n", ~((unsigned long) ~0 >> 1), (unsigned long) ~0 >> 1);
	printf("unsigned long \t%lu, %lu\n", 0, ULONG_MAX);
	printf("unsigned long \t%lu, %lu\n", 0, (unsigned long) ~0);

	return 0;
}
