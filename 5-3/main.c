#include <stdio.h>
#define MAXLINE 100

int mygetline(char s[], int lim);
void mystrcat(char *s, char *t);

/* Exercise 5-3. Write a pointer version of the function strcat that we showed
 * in Chapter 2: strcat(s,t) copies the string t to the end of s. */

int
main()
{
	char line1[2*MAXLINE], line2[MAXLINE];
	while (mygetline(line1, 2*MAXLINE)) {
		mygetline(line2, MAXLINE);
		mystrcat(line1, line2);
		printf("\n%s\n", line1);
	}

	return 0;
}

/* strcat: concatenate t to end of s; s must be big enough */
void
mystrcat(char s[], char t[])
{
	for ( ; *s != '\0'; s++) /* find end of s */
		;
	for ( ; (*s = *t) != '\0'; s++, t++) /* copy t */
		;
}

/* mygetline: get line into s, return length */
int
mygetline(char s[], int lim)
{
	int c, i;

	i = 0;
	while (--lim > 0 && (c=getchar()) != EOF && c != '\n')
		s[i++] = c;
	if (c == '\n')
		s[i++] = c;
	s[i] = '\0';
	return i;
}
