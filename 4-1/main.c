#include <stdio.h>
#include <string.h>
#define MAXLINE 1000 /* maximum input line length */

/* Exercise 4-1. Write the function strrindex(s,t), which returns the position
 * of the rightmost occurrence of t in s, or -1 if there is none. */

int mygetline(char line[], int max);
int strindex(char source[], char searchfor[]);

char pattern[] = "ould"; /* pattern to search for */

/* find all lines matching pattern */
int
main()
{
	char line[MAXLINE];

	while (mygetline(line, MAXLINE) > 0)
		printf("%d: %s", strindex(line, pattern), line);

	return 0;
}

/* mygetline: get line into s, return length */
int
mygetline(char s[], int lim)
{
	int c, i;

	i = 0;
	while (--lim > 0 && (c=getchar()) != EOF && c != '\n')
		s[i++] = c;
	if (c == '\n')
		s[i++] = c;
	s[i] = '\0';
	return i;
}

/* strindex: return the position of the rightmost
 * occurrence of t in s, or -1 if there is none */
int
strindex(char s[], char t[])
{
	int i, j, k;

	for (i = strlen(s) - 1; i >= 0; i--) {
		for (j = i, k = strlen(t) - 1; j >= 0 && k >= 0 && s[j] == t[k]; j--, k--)
			;
		if (k < 0 && j != i) /* t[] is not empty */
			return ++j;
	}
	return -1;
}
