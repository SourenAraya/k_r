#include <stdio.h>
#include <stdlib.h>

/* states */
#define PRE_ESC_SEQ -2
#define ESC_SEQ -1
#define OUT 0
#define SINGLE_QUOTE 1
#define DOUBLE_QUOTE 2
#define PRE_COMMENT 3
#define COMMENT 4
#define PRE_END_COMMENT 5
#define END_COMMENT 6
#define CURLY_BRACKETS 7
#define SQUARE_BRACKETS 8
#define ROUND_BRACKETS 9

#define STACKSIZE 1000

typedef struct stack
{
	int values[STACKSIZE];
	int size;
} stack;

void c_checker(void);
void push(stack *ps, int state);
int pop(stack *ps);
int read_pop(stack *ps);


/* Exercise 1-24. Write a program to check a C program for rudimentary syntax
 * errors like unbalanced parentheses, brackets and braces. Don’t forget about
 * quotes, both single and double, escape sequences, and comments. (This pro-
 * gram is hard if you do it in full generality.) */

int
main()
{
	c_checker();

	return 0;
}

/* push() and pop() are copy-paste from russian wikipedia */
void push(stack *ps, int state) // Добавление в стек нового элемента
{
	if ( ps->size == STACKSIZE) { // Не переполнен ли стек?
		fputs( "Error: stack overflow\n", stderr);
		abort();
	}
	else {
		ps->values[ps->size++] = state;
	}
}

int pop(stack *ps) // Удаление из стека
{
	if (ps->size == 0) { // Не опустел ли стек?
		fputs("Error: stack underflow\n", stderr);
		abort();
	}
	else {
		return ps->values[--ps->size];
	}
}

int read_pop(stack *ps) // Чтение без yдаления из стека
{
	if (ps->size == 0) { // Не опустел ли стек?
		fputs("Error: stack underflow\n", stderr);
		abort();
	}
	else {
		return ps->values[(ps->size) - 1];
	}
}

/* check a C program for rudimentary syntax errors like 
 * unbalanced parentheses, brackets and braces */
void
c_checker(void)
{
	int c;
	int state = OUT; /* initial state */
	stack states;
	states.size = 0; /* there are no elements in the stack yet */
	push(&states, state);

	while ((c = getchar()) != EOF) {
		if ((state == PRE_COMMENT && c != '*') || state == END_COMMENT) { /* if it is not a comment anymore/yet then return to non-comment state*/
			/* delete all COMMENT-related states and also one state before them */
			while ((state = pop(&states)) == PRE_COMMENT || state == COMMENT || state == PRE_END_COMMENT || state == END_COMMENT);
			push(&states, state); /* return last deleted state */
		}
		else if (state == PRE_END_COMMENT && c != '/') /* if it is not ending of a comment */
			push(&states, state = COMMENT);

		if (state == PRE_ESC_SEQ)
			push(&states, state = ESC_SEQ);
		else if (state == ESC_SEQ) {
			while ((state = pop(&states)) == ESC_SEQ || state == PRE_ESC_SEQ); /* delete all ESC-related stated and also one before them */
			push(&states, state); /* return last deleted state */
		}

		if (c == '\\') {
			if (state == SINGLE_QUOTE || state == DOUBLE_QUOTE) /* if next character is a part of esc sequence like '\'' */
				push(&states, state = PRE_ESC_SEQ);
		}
		else if (c == '\'') {
			if (state == OUT || state == CURLY_BRACKETS || state == ROUND_BRACKETS || state == SQUARE_BRACKETS)
				push(&states, state = SINGLE_QUOTE);
			else if (state == SINGLE_QUOTE) {
				pop(&states);
				state = read_pop(&states);
			}
		}
		else if (c == '\"') {
			if (state == OUT || state == CURLY_BRACKETS || state == ROUND_BRACKETS || state == SQUARE_BRACKETS)
				push(&states, state = DOUBLE_QUOTE);
			else if (state == DOUBLE_QUOTE) {
				pop(&states);
				state = read_pop(&states);
			}
		}
		else if (c == '*') {
			if (state == PRE_COMMENT) /* if it is an opening of a comment */
				push(&states, state = COMMENT);
			else if (state == COMMENT) /* if it is a probable closing of a comment */
				push(&states, state = PRE_END_COMMENT);
		}
		else if (c == '/') {
			if (state == OUT || state == CURLY_BRACKETS || state == ROUND_BRACKETS || state == SQUARE_BRACKETS) /* if it is a probable opening of a comment */
				push(&states, state = PRE_COMMENT);
			else if (state == PRE_END_COMMENT) /* if it is a closing of a comment */
				push(&states, state = END_COMMENT);
		}
		else if (c == '{') {
			if (state != COMMENT && state != SINGLE_QUOTE && state != DOUBLE_QUOTE)
				push(&states, state = CURLY_BRACKETS);
		}
		else if (c == '(') {
			if (state != COMMENT && state != SINGLE_QUOTE && state != DOUBLE_QUOTE)
				push(&states, state = ROUND_BRACKETS);
		}
		else if (c == '[') {
			if (state != COMMENT && state != SINGLE_QUOTE && state != DOUBLE_QUOTE)
				push(&states, state = SQUARE_BRACKETS);
		}
		else if (c == '}') {
			if (state == CURLY_BRACKETS) {
				pop(&states);
				state = read_pop(&states);
			}
			else if (state == ROUND_BRACKETS || state == SQUARE_BRACKETS)
				printf("\nError: missing '{' \n");
		}
		else if (c == ')') {
			if (state == ROUND_BRACKETS) {
				pop(&states);
				state = read_pop(&states);
			}
			else if (state == CURLY_BRACKETS || state == SQUARE_BRACKETS)
				printf("\nError: missing '(' \n");
		}
		else if (c == ']') {
			if (state == SQUARE_BRACKETS) {
				pop(&states);
				state = read_pop(&states);
			}
			else if (state == CURLY_BRACKETS || state == ROUND_BRACKETS)
				printf("\nError: missing '[' \n");
		}

		/* if (c == '\n')
			printf("\t state = %d", state); */

		putchar(c);
	}

	if (states.size != 0) {
		while ((state = pop(&states)) != OUT) {
			if (state == CURLY_BRACKETS)
				printf("Error: missing '}' \n");
			else if (state == ROUND_BRACKETS)
				printf("Error: missing ')' \n");
			else if (state == SQUARE_BRACKETS)
				printf("Error: missing ']' \n");
		}
	}
}
