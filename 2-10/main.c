#include <stdio.h>
#include <stdlib.h>
#define MAXLINE 100

int mygetline(char s[], int lim);
int lower(int c);

/* Exercise 2-3. Write the function htoi(s) , which converts a string of hexa-
 * decimal digits (including an optional 0x or 0X ) into its equivalent integer value.
 * The allowable digits are 0 through 9 , a through f , and A through F.*/

int main()
{
	int len;
	char line[MAXLINE];

	while ((len = mygetline(line, MAXLINE)) > 1) { /* more then 1 - there was a line */
		for (int i = 0; line[i] != '\0'; ++i)
			putchar(lower(line[i]));
		putchar('\n');
	}

	return 0;
}

/* getline: read a line into s, return length */
int mygetline(char s[], int lim)
{
	int c, i;

	for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c != '\n'; ++i)
		s[i] = c;

	if (c == '\n')
		s[i++] = c;
	s[i] = '\0';

	return i;
}

/* lower: convert c to lower case; ASCII only */
int lower(int c)
{
	return (c >= 'A' && c <= 'Z') ? c + 'a' - 'A' : c;
}
