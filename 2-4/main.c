#include <stdio.h>
#define MAXLINE 100

int mygetline(char s[], int lim);
int squeeze(char s1[], const char s2[]);

/* Exercise 2-4. Write an alternate version of squeeze(s1,s2) that deletes
 * each character in s1 that matches any character in the string s2. */

int main()
{
	char line1[MAXLINE], line2[MAXLINE];

	/* initialize line1 */
	printf("line 1: ");
	mygetline(line1, MAXLINE);
	printf("\n"); /* in case user entered line without \n */
	
	/* initialize line2 */
	/* it may be appropriate to end entering with EOF instead of ENTER */
	printf("line 2 ('\\n' also counts): ");
	mygetline(line2, MAXLINE);
	printf("\n"); /* in case user entered line without \n */

	/* squeeze line1 */
	squeeze(line1, line2);
	printf("line 3: %s\n", line1);

	return 0;
}

/* getline: read a line into s, return length */
int mygetline(char s[], int lim)
{
	int c, i;

	for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c != '\n'; ++i)
		s[i] = c;
	if (c == '\n') {
		s[i] = c;
		++i;
	}
	s[i] = '\0';
	return i;
}

/* squeeze: delete each character in s1 that matches any character
 * in the string s2, return new length of s1 */
int squeeze(char s1[], const char s2[])
{
	int i, j, k;

	for (i = j = 0; s1[i] != '\0'; i++) {
		for (k = 0; s1[i] != s2[k] && s2[k] != '\0'; k++);
		if (s2[k] == '\0')
			s1[j++] = s1[i];
	}
	s1[j] = '\0';
	return j;
}
