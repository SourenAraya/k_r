#include <stdio.h>

#define IN 1 /* inside a word */
#define OUT 0 /* outside a word */
#define N 100

/* Exercise 1-13. Write a program to print a histogram of the lengths of words
in its input. It is easy to draw the histogram with the bars horizontal; a vertical
orientation is more challenging.*/

int main()
{
	int c, i, j, state, p[N];
	int words = 0;//number of words
	state = OUT;//not in a word

	//init p[] with zeros for future increments of p[] elements
	for (i=0;i<N;i++)
		p[i]=0;

	//init p[] with word's lengths
	while ((c = getchar()) != EOF) {
		if (c == ' ' || c == '\n' || c == '\t') {
			if (state == IN) {
				state = OUT;
				words++;
			}
		}
		else {
			if (state == OUT)
				state = IN;
			p[words]++;
		}
	}

	//print a horizontal histogram
	for (i=0; i < words; i++){
		for (j=0;j < p[i]; j++)
			putchar('-');
		putchar('\n');
	}

	putchar('\n');

	//biggest value of p[words]
	int biggest = 0;
	for (i=0; i < words; i++)
	   if (p[i] > biggest)
		   biggest = p[i];

	//print a downward vertical histogram
	for (i=0; i < biggest; i++) {//lines
		for (j=0; j < words; j++)//rows
			if (p[j] > i)
				putchar('|');
			else
				putchar(' ');
		putchar('\n');
	}

	putchar('\n');

	//print an upward vertical histogram
	for (i = biggest - 1; i >= 0; i--) {//lines
		for (j=0; j < words; j++)//rows
			if (p[j] > i)
				putchar('|');
			else
				putchar(' ');
		putchar('\n');
	}

	return 0;
}
