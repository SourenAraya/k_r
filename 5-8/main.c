#include <stdio.h>
#include <string.h>

#define MAXLEN 1000 /* max length of any input line */

int mygetline(char *, int);
int day_of_year(int year, int month, int day);
void month_day(int year, int yearday, int *pmonth, int *pday);

/* Exercise 5-8. There is no error checking in day_of_year or month_day.
 * Remedy this defect. */

int
main()
{
	int year, month, day, count;

	printf("Number of cases = ");
	scanf("%d", &count);
	getchar();
	while (count--) {
		printf("\n(Year and year-day to year, month, day)\n");
		printf("Enter year and year-day divided by space: ");
		scanf("%d %d", &year, &day);
		getchar();
		month_day(year, day, &month, &day);
		printf("year = %d, month = %d, day = %d\n\n", year, month, day);

		printf("(Year, month, day to year and year-day)\n");
		printf("Enter year, month and day divided by space: ");
		scanf("%d %d %d", &year, &month, &day);
		getchar();
		printf("year = %d, day of year = %d\n", year, day_of_year(year, month, day));
	}
}

static char daytab[2][13] = {
	{0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31},
	{0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}
};

/* day_of_year: set day of year from month & day */
int
day_of_year(int year, int month, int day)
{
	int i, leap;

	leap = year % 4 == 0 && year % 100 != 0 || year % 400 == 0;

	if (year < 1 || month < 1 || month > 12 || day < 1 || day > daytab[leap][month]) {
		printf("Error: incorrect value(s)\n");
		return -1;
	}

	for (i = 1; i < month; i++)
		day += daytab[leap][i];

	return day;
}

/* month_day: set month, day from day of year */
void
month_day(int year, int yearday, int *pmonth, int *pday)
{
	int i, leap;

	leap = year % 4 == 0 && year % 100 != 0 || year % 400 == 0;

	if (year < 1 || yearday < 1 || (leap && yearday > 366) || (!leap && yearday > 365)) {
		printf("Error: incorrect value(s)\n");
	   	yearday = -1;
	}

	for (i = 1; yearday > daytab[leap][i]; i++)
		yearday -= daytab[leap][i];

	*pmonth = i;
	*pday = yearday;
}

/* getline: read a line into s, return length */
int
mygetline(char s[], int lim)
{
	int c, i;

	for (i = 0; i < lim-1 && (c=getchar()) != EOF && c != '\n'; ++i)
		s[i] = c;
	if (c == '\n') {
		s[i] = c;
		++i;
	}
	s[i] = '\0';
	return i;
}
