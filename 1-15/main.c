#include <stdio.h>

/* Exercise 1-15. Rewrite the temperature conversion program of Section 1.2 to
 * use a function for conversion. */

float fahr_to_cels(int celsius);

int main()
{
	int fahr;
	printf("fahr celsius\n");
	for (fahr = 0; fahr <= 300; fahr = fahr + 20)
		printf("%4d %7.1f\n", fahr, fahr_to_cels(fahr));

	return 0;
}

/* conversion Fahrenheit to Celsius */
float fahr_to_cels(int fahrenheit)
{
	return (5.0/9.0)*(fahrenheit-32);
}
