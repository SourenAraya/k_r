#include <stdio.h>
#include <string.h>
#define MAXLINE 100
#define NUMBER '0' /* signal that a number was found by getop() */

int mygetline(char *s, int lim);
int myatoi(char *s);
void myitoa(int n, char *s);
void myreverse(char *s);
int mystrindex(char *s, char *t);
int getop(char *s);

/* Exercise 5-6. Rewrite appropriate programs from earlier chapters and exercises
 * with pointers instead of array indexing. Good possibilities include getline
 * (Chapters 1 and 4), atoi, itoa, and their variants (Chapters 2, 3, and 4),
 * reverse (Chapter 3), and strindex and getop (Chapter 4). */

int
main()
{
	int num = 0, len = 0;
	char line1[MAXLINE*2] = "", line2[MAXLINE] = "";

	/* testing getop */
	printf("type number or expression:\n");
	while ((getop(line2)) != EOF)
		if (line2[0] != '\n')
			strcat(line1, line2);
	printf("\n%s\n", line1);

	printf("type number: ");
	mygetline(line2, MAXLINE);

	/* testing myatoi */
	num = myatoi(line2);
	printf("atoi: number = %d\n", num);

	/* testing myitoa */
	myitoa(num, line2);
	printf("itoa: number = %s\n", line2);

	/* testing myreverse */
	myreverse(line2);
	printf("reverse: number = %s\n", line2);

	/* testing mystrindex */
	printf("Substring to search for in the string \"%s\": ", line1);
	mygetline(line2, MAXLINE);
	if ((len = strlen(line2)) && line2[len - 1] == '\n')
		line2[len - 1] = '\0'; /* get rid of '\n' in line2 */
	printf("index of substring (line2) in line1: %d\n", mystrindex(line1, line2));

	return 0;
}

/* mygetline: get line into s, return length */
int
mygetline(char *s, int lim)
{
	int c;
	char *t = s;

	while (--lim > 0 && (c=getchar()) != EOF && c != '\n')
		*s++ = c;

	if (c == '\n')
		*s++ = c;

	*s = '\0';

	return t - s;
}

#include <ctype.h>

/* atoi: convert s to integer */
int
myatoi(char *s)
{
	int i, n, sign;

	for (i = 0; isspace(*s); s++) /* skip white space */
		;

	sign = (*s == '-') ? -1 : 1;

	if (*s == '+' || *s == '-') /* skip sign */
		s++;

	for (n = 0; isdigit(*s); s++)
		n = 10 * n + (*s - '0');

	return sign * n;
}

/* itoa: convert n to characters in s */
void
myitoa(int n, char *s)
{
	char *s_orig = s;
	int sign;

	if ((sign = n) < 0)		/* record sign */
		n = -n;				/* make n positive */

	do {		/* generate digits in reverse order */
		*s++ = n % 10 + '0';	/* get next digit */
	} while ((n /= 10) > 0);	/* delete it */

	if (sign < 0)
		*s++ = '-';

	*s = '\0';

	myreverse(s_orig);
}

#include <string.h>

/* reverse: reverse string s in place */
void
myreverse(char *s)
{
	int c;
	char *t = s + strlen(s) - 1;

	for ( ; s < t; s++, t--) {
		c = *s, *s = *t, *t = c;
	}
}

/* strindex: return index of t in s, -1 if none */
int
mystrindex(char *s, char *t)
{
	const char *s_orig = s;
	char *a, *b;

	for ( ; *s != '\0'; s++) {
		for (a = s, b = t; *b != '\0' && *a == *b; a++, b++)
			;
		if (b > t && *b == '\0')
			return s - s_orig;
	}
	return -1;
}

#include <ctype.h>

int mygetch(void);
void myungetch(int);

/* getop: get next operator or numeric operand */
int
getop(char *s)
{
	int i, c;
	while ((*s = c = mygetch()) == ' ' || c == '\t')
		;
	*(s + 1) = '\0';
	if (!isdigit(c) && c != '.')
		return c; /* not a number */
	i = 0;
	if (isdigit(c)) /* collect integer part */
		while (isdigit(*++s = c = mygetch()))
			;
	if (c == '.') /* collect fraction part */
		while (isdigit(*++s = c = mygetch()))
			;
	*s = '\0';
	if (c != EOF)
		myungetch(c);
	return NUMBER;
}

#define BUFSIZE 100

char buf[BUFSIZE]; /* buffer for ungetch */
int bufp = 0; /* next free position in buf */

int
mygetch(void) /* get a (possibly pushed back) character */
{
	return (bufp > 0) ? *(buf + --bufp) : getchar();
}

void
myungetch(int c) /* push character back on input */
{
	if (bufp >= BUFSIZE)
		printf("ungetch: too many characters\n");
	else
		*(buf + bufp++) = c;
}
