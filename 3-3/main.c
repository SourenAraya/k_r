#include <stdio.h>
#include <ctype.h>

#define MAXLINE 100

/* OTHER is an esc_sequence like \n, or symbol like coma or space */
enum states { OTHER, OUT, 
	LOWER_CASE, LOWER_CASE_PRE_SEQ, LOWER_CASE_SEQ, 
	UPPER_CASE, UPPER_CASE_PRE_SEQ, UPPER_CASE_SEQ, 
	NUMBER, NUMBER_PRE_SEQ, NUMBER_SEQ };

/* Exercise 3-3. Write a function expand(s1,s2) that expands shorthand nota-
 * tions like a-z in the string s1 into the equivalent complete list abc ... xyz in
 * s2 . Allow for letters of either case and digits, and be prepared to handle cases
 * like a-b-c and a-z0-9 and -a-z . Arrange that a leading or trailing - is
 * taken literally. */

/* In my implementations of expand(s1,s2)
 * s1 = "-a-c--a9-0-0-b00-0-" will produce s2 = "-abc--a9876543210-b00-" */

int mygetline(char s[], int lim);
int expand(const char s1[], char s2[]);
enum states expand_parser(enum states state, char c);

int
main()
{
	char line1[MAXLINE], line2[MAXLINE];

	while (mygetline(line1, MAXLINE) > 1) { /* there was a non empty line */
		expand(line1, line2); /* copy line1 to line2, expanding sequences */
		printf("%s", line2);
	}

	return 0;
}

/* getline: read a line into s, return length */
int
mygetline(char s[], int lim)
{
	int c, i;

	for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c != '\n'; ++i)
		s[i] = c;
	if (c == '\n')
		s[i++] = c;

	s[i] = '\0';
	return i;
}

/* expand: expands shorthand notations like a-z in the string s1
 * into the equivalent complete list abc ... xyz in s2. 
 * Return length of s2. */
int
expand(const char s1[], char s2[])
{
	int i = 0, j = 0;
	enum states pre_state = OUT, state = OUT;

	do {
		pre_state = state;
		state = expand_parser(state, s1[i]);

		/* s1[i] not part of a sequence */
		if (state == LOWER_CASE || state == UPPER_CASE || state == NUMBER || state == OTHER || state == OUT) {
			/* s1[i] appeared to be not part of a sequence */
			if (pre_state == LOWER_CASE_PRE_SEQ || pre_state == UPPER_CASE_PRE_SEQ || pre_state == NUMBER_PRE_SEQ)
				s2[j++] = s1[i - 1]; /* copy '-' */
			s2[j++] = s1[i];
		}
		/* s[i] is part of a sequence: temp, temp + 1, .., s[i] */
		else if (state == LOWER_CASE_SEQ || state == UPPER_CASE_SEQ || state == NUMBER_SEQ) {
			if (s2[j - 1] < s1[i]) /* ascending sequence */
				for (int temp = s2[j - 1] + 1; temp <= s1[i]; temp++) /* expand sequence */
					s2[j++] = temp;
			else if (s2[j - 1] > s1[i]) /* descending sequence */
				for (int temp = s2[j - 1] - 1; temp >= s1[i]; temp--) /* expand sequence */
					s2[j++] = temp;
			/* if s2[j - 1] == s1[i] then do nothing. Sequence like 9-9-9 will produce 9 */
		}
	} while (s1[i++] != '\0');

	return j;
}

/* expand_parser: return current state, defined by enum states */
enum states
expand_parser(enum states state, char c)
{
	if (islower(c)) {
		if (state == LOWER_CASE_PRE_SEQ)
			return LOWER_CASE_SEQ;
		else
			return LOWER_CASE;
	}
	else if (isupper(c)) {
		if (state == UPPER_CASE_PRE_SEQ)
			return UPPER_CASE_SEQ;
		else
			return UPPER_CASE;
	}
	else if (isdigit(c)) {
		if (state == NUMBER_PRE_SEQ)
			return NUMBER_SEQ;
		else
			return NUMBER;
	}
	else if (c == '-') {
		if (state == LOWER_CASE || state == LOWER_CASE_SEQ)
			return LOWER_CASE_PRE_SEQ;
		else if (state == UPPER_CASE || state == UPPER_CASE_SEQ)
			return UPPER_CASE_PRE_SEQ;
		else if (state == NUMBER || state == NUMBER_SEQ)
			return NUMBER_PRE_SEQ;
		else /* state == *PRE_SEQ or OUT or OTHER*/
			return OUT;
	}
	else /* if c is an esc_sequence like \n, or symbol like coma or space */
		return OTHER;
}
