#include <stdio.h>
#include <string.h>

#define MAXLINE 100

/* Exercise 3-6. Write a version of itoa that accepts three arguments instead of
 * two. The third argument is a minimum field width; the converted number must
 * be padded with blanks on the left if necessary to make it wide enough. */

void itoa(int n, char s[], int w);
void reverse(char s[]);

int
main()
{
	int number = 0, minw = 0;
	char line[MAXLINE];

	printf("number = ");
	scanf("%d", &number);

	printf("minimum width = ");
	scanf("%d", &minw);

	itoa(number, line, minw); /* convert number into string */
	printf("%s\n", line);

	return 0;
}

/* itoa: convert n to characters in s */
/* w - minimum field width, if n is too short than it precedes with '0' in s */
void
itoa(int n, char s[], int w)
{
	int i, sign;
	unsigned int nn = n;

	if ((sign = n) < 0) /* record sign */
		nn = -n;         /* make nn positive */
	i = 0;
	do {       /* generate digits in reverse order */
		s[i++] = nn % 10 + '0';   /* get next digit */
		w--; /* how many '0' to insert when the end of a number is reached */
	} while ((nn /= 10) > 0 || w > 0);     /* delete it */
	if (sign < 0)
		s[i++] = '-';
	s[i] = '\0';
	reverse(s);
}

/* reverse: reverse string s in place */
void 
reverse(char s[])
{
	int c, i, j;
	for (i = 0, j = strlen(s)-1; i < j; i++, j--)
		c = s[i], s[i] = s[j], s[j] = c;
}
